var express = require('express');
var app = express();
var modelArtist = require('../models/model.getartist.js');

exports.get = app.get('/getartist', function (req, res) {
    modelArtist.dbGetArtistList(function(result){
        res.end( JSON.stringify(result));
    });
 });

 var server = app.listen(8081, function () {
    var port = server.address().port
    console.log("Example app listening at http://localhost:%s", port)
 });