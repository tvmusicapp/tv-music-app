const MYSQL = require('mysql');
var e;
const TABLES = {
    tb_playlist:'',
    tb_playlist_data:'',
    song:'song',
    tbl_artist:'tbl_artist',
    tbl_artist_song:'tbl_artist_song',
    tbl_top_list:'tbl_top_list'
};
const HOST = "localhost";
const USER = "root";
const PASSWORD = "";
const DATABASE =  "playlist";
const CONNECT_DB = MYSQL.createConnection({
    host: HOST,
    user: USER,
    password: PASSWORD,
    database: DATABASE
  }); 

exports.e = {
    TABLES:TABLES,
    CONNECT_DB:CONNECT_DB 
 } 
  