var express = require('express');
var app = express();
var fs = require("fs");
var mysql = require('mysql');

require('./controllers/controller.getartist.js');


app.get('/songdetails/:id', function (req, res) {
   fs.readFile( __dirname + "/" + "api.songs.details.json", 'utf8', function (err, data) {
    var songs = JSON.parse( data );
    var song  = songs.filter(function(i){
        return (i.id == req.params.id);
    })
    //var song = songs["song" + req.params.id] 
    res.end( JSON.stringify(song));
 });
})



app.get('/toplist', function (req, res) {
    var con = mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "",
        database: "music_app_top_list"
      });  
  con.connect(function(err) {
    if (err) throw err;
    con.query("SELECT * FROM music_app_top_list LIMIT 20", function (err, result) {
      if (err) throw err;
       res.end( JSON.stringify(result));
    });
  });
 })

