import React from 'react';
import { View } from 'react-native';
import { Router, Scene } from 'react-native-router-flux'
import ListArtists from './src/screens/ListArtists';
import ListSongs from './src/screens/ListSongs';
import Player from './src/screens/Player';

const Routes = () => (
    <Router>
        <Scene key="root">
            <Scene key="ListArtists" id="artists" hideNavBar={true}  component={ListArtists} title="ListArtists" initial={true} />
            <Scene key="ListSongs" id="songs" hideNavBar={true}  component={ListSongs} title="ListSongs" />
            <Scene key="Player" id="player"  hideNavBar={true}  component={Player} title="Player" />
        </Scene>
    </Router>
)
export default Routes