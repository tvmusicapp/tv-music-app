import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10,
    padding: 10,
    margin: 50
  },
  imageWrapper:{
    borderWidth:1,
    borderColor:"#ddd",
    borderRadius:75,
    height: 150,
    width: 150,
    position:'relative'
  },
  image: {
    height: 138,
    width: 138,
    backgroundColor: '#859a9b',
    borderRadius: 69,
    padding: 20,
    margin:5,
    shadowColor: '#303838',
    position:'absolute',
    left:0,
    top:0
  },
  overlay:{
    height: 138,
    width: 138,
    borderRadius: 69,
    margin:5,
    backgroundColor: 'rgba(0,0,0,.5)',
    position:'absolute',
    left:0,
    top:0,
  },
  singerName:{
    position:'absolute',
    zIndex:2,
    color:'#ffffff',
    top:65,
    left:10,
    textAlign:'center',
    width:125,
    fontWeight:'bold'
  },
  musiclogo: {
    textAlign: 'right',
    display: 'flex'
  },
  listItem: {
    display: 'flex',
    flexDirection: 'row',
    borderWidth: 1,
    borderRadius: 5,
    margin: 2,
    borderColor: '#ddd',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10,
    backgroundColor: '#fff',
    borderColor: '#d3d3d3',
    borderBottomWidth: 0,
    elevation: 1,
  },

  active: {
    backgroundColor: '#fcede3',
    color: '#fd6331',
    borderBottomColor: '#fd6331',
    borderBottomWidth: 5,
    elevation: 3,
  },
  touchableOpacity: {

  },
  titleWrapper: {
    display: 'flex',
    flexDirection: 'column',
  },
  listItemImages: {
    width: 70,
    height: 70,
    margin: 5
  },
  titleHead: {
    alignSelf: 'stretch',
    fontWeight: 'bold'
  },
  activeTitleHead: {
    color: '#fd6331',
  },
  duration: {
    alignSelf: 'stretch',
    fontSize: 12
  },
  artistName: {
    alignSelf: 'stretch'
  },
  listhHeader: {
    display: 'flex',
    flexDirection: 'row',
    borderRadius: 5,
    justifyContent: 'space-between',
    padding: 10,
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 0,
    elevation: 1
  },
  listhHeaderGradient: {
    borderRadius: 25,
    padding: 5
  },
  profile_box: {
    borderWidth: 0,
    minWidth: '20%',
    flexDirection: 'row',
    color: "#fff"
  },
  profileImage: {
    width: 25,
    height: 25,
    borderRadius: 25
  },
  profileTitle: {
    color: '#fff',
    padding: 3,
    fontWeight: 'bold'
  },
  rsiMusicLogo: {
    textAlign: 'right',
    alignSelf: 'stretch'
  },
  linearGradient: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 5
  },
  buttonText: {
    fontSize: 18,
    fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent',
  }


});
export default styles;
