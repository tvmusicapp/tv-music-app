import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import styles from '../../Style';
import GridView from 'react-native-super-grid';
import { Actions } from 'react-native-router-flux';


export default class ListGridView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            artists: this.props.items,
            itemDimension: this.props.itemDimension ? this.props.itemDimension : 200
        }
    }

    componentDidMount() {
    }
    playMusicByTrackId(id, url) {
        //alert("track_id->" + id + "--url->" + url);
        Actions.Player();
    }
    navigateSongListPage() {
        Actions.ListSongs();
    }
    render() {

        return (
            <View>
                <GridView
                    itemDimension={this.state.itemDimension}
                    items={this.state.artists}
                    renderItem={
                        item => (
                            this.props.id === "songs" ?
                                <View>
                                    <TouchableOpacity
                                        key={item.track_id}
                                        hasTVPreferredFocus={(this.props.activeTrackId === item.track_id) && true}
                                        style={[styles.listItem, (this.props.activeTrackId === item.track_id) && styles.active]}
                                        onPress={() => this.playMusicByTrackId(item.track_id, item.url)}>
                                        <Image source={{ uri: item.image_path }} style={styles.listItemImages} />
                                        <View style={styles.titleWrapper}>
                                            <Text style={[styles.titleHead, (this.props.activeTrackId === item.track_id) && styles.activeTitleHead]}>
                                                {item.song_title}
                                            </Text>
                                            <Text style={styles.artistName}>
                                                {item.artist_name}
                                            </Text>
                                            <Text style={styles.duration}>
                                                {item.duration}
                                            </Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                : this.props.id === "artists" ?

                                    <TouchableOpacity onPress={() => this.navigateSongListPage()}
                                        hasTVPreferredFocus={(this.props.activeTrackId === item.track_id) && true}
                                    >
                                        <View style={styles.imageWrapper} >
                                            <Image source={{ uri: item.image_path }} style={styles.image} />
                                            <Text style={styles.singerName}>{this.props.activeTrackId === item.track_id && item.artist_name} </Text>
                                            <Text style={this.props.activeTrackId === item.track_id && styles.overlay} />
                                        </View>
                                    </TouchableOpacity>
                                    :
                                    <View>
                                        <Text>This is default page</Text>
                                    </View>

                        )
                    }
                />

            </View>
        );
    }
}
