import { StyleSheet } from "react-native";

const styleGridView = StyleSheet.create({
    welcome: {
        fontSize: 40,
        textAlign: 'center',
        margin: 10,
    }
});

export default styleGridView