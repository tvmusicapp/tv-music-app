const Images = {
    profile: {
        artist1: require('../../images/artist1.jpeg'),
        artist2: require('../../images/artist2.jpeg'),
        artist3: require('../../images/artist3.jpeg'),
        artist4: require('../../images/artist4.jpeg')
    },
    musicLogo: require('../../images/music-logo.png'),
    songList: require('../../images/Song-list.jpg')
};

export default Images;