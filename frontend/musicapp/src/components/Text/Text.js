import React from 'react'
import {Text} from 'react-native';

import style from "./style";

export const CustomText = props => {
    const { text } = props;
    return (
        <Text>{text} </Text>
    )
}
