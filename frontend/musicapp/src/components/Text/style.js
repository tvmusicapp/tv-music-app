import { StyleSheet } from "react-native";

const styleText = StyleSheet.create({
    welcome: {
        fontSize: 40,
        textAlign: 'center',
        margin: 10,
    }
});

export default styleText