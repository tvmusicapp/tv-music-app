import React, { Component } from 'react';
import { View } from 'react-native';
import ListGridView from '../components/GridView/ListGridView';
import artistsJson from '../mock/artists.json';

export default class ListArtists extends Component {
    constructor(props) {
        super(props);
        this.state = {
            artists: []
        }
    }

    componentWillMount() {
        this.setState({ artists: artistsJson, activeTrackId: 1 });
    }

    render() {
        return (
            this.state.artists && <View>
                <ListGridView id={this.props.id} itemDimension={150} activeTrackId={this.state.activeTrackId} items={this.state.artists} />
            </View>
        );
    }
}