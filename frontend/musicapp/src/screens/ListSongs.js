import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import styles from '../Style';
import ListGridView from '../components/GridView/ListGridView';
import songsJson from '../mock/songs.json';
import Images from '../components/Images/Images';
import LinearGradient from 'react-native-linear-gradient';
export default class ListSongs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            songs: [],
            profile_title: '',
            profile_image: ''
        }
    }

    componentWillMount() {
        this.setState({ songs: songsJson, activeTrackId: 3, profile_title: 'Top20 list', profile_image: Images.profile.artist1 });


    }
    render() {
        return (
            this.state.songs && <View>
                <View style={styles.listhHeader} >
                    <LinearGradient
                        colors={['#fd5228', '#fe954a']}
                        start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 1.0 }}
                        style={styles.listhHeaderGradient}
                    >
                        <View style={styles.profile_box} >
                            <Image source={this.state.profile_image} style={styles.profileImage} />
                            <Text style={styles.profileTitle}> {this.state.profile_title} </Text>
                        </View>
                    </LinearGradient>
                    <Image source={Images.musicLogo} style={styles.rsiMusicLogo} />
                </View>
                <ListGridView id={this.props.id} itemDimension={200} activeTrackId={this.state.activeTrackId} items={this.state.songs} />
            </View>
        );
    }
}