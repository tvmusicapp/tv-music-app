-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 09, 2018 at 03:22 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `playlist`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `filterTodo` (IN `done` BOOLEAN)  BEGIN
    SELECT * FROM todos WHERE completed = done;
END$$

--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `CustomerLevel` (`p_creditLimit` DOUBLE) RETURNS VARCHAR(10) CHARSET latin1 BEGIN
    DECLARE lvl varchar(10);
 
    IF p_creditLimit > 50000 THEN
 SET lvl = 'PLATINUM';
    ELSEIF (p_creditLimit <= 50000 AND p_creditLimit >= 10000) THEN
        SET lvl = 'GOLD';
    ELSEIF p_creditLimit < 10000 THEN
        SET lvl = 'SILVER';
    END IF;
 
 RETURN (lvl);
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `playlist`
--

CREATE TABLE `playlist` (
  `playlist_id` int(11) NOT NULL,
  `user_id` char(100) NOT NULL,
  `title` char(100) NOT NULL,
  `temp` char(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `playlist`
--

INSERT INTO `playlist` (`playlist_id`, `user_id`, `title`, `temp`) VALUES
(11, '1234', 'hindi', 'null'),
(12, '1234', 'cute', 'null');

-- --------------------------------------------------------

--
-- Table structure for table `playlist_data`
--

CREATE TABLE `playlist_data` (
  `id` int(11) NOT NULL,
  `playlist_id` int(11) NOT NULL,
  `song_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `song`
--

CREATE TABLE `song` (
  `id` int(11) NOT NULL,
  `song_title` char(100) NOT NULL DEFAULT 'Song 1',
  `album_id` int(11) NOT NULL,
  `url` char(200) NOT NULL DEFAULT 'http://song.path',
  `image_path` char(200) NOT NULL DEFAULT 'http://image.path'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `song`
--

INSERT INTO `song` (`id`, `song_title`, `album_id`, `url`, `image_path`) VALUES
(1, 'Song 1', 1, 'http://song.path', 'http://image.path'),
(2, 'Song 2', 1, 'http://song.path', 'http://image.path'),
(3, 'Song 1', 1, 'http://song.path', 'http://image.path'),
(4, 'Song 2', 1, 'http://song.path', 'http://image.path'),
(5, 'Song 2', 0, 'http://song.path', 'http://image.path'),
(6, 'Song 3', 0, 'http://song.path', 'http://image.path'),
(7, 'Song 4', 2, 'http://song.path', 'http://image.path'),
(8, 'Song 3', 2, 'http://song.path', 'http://image.path'),
(9, 'Song 6', 3, 'http://song.path', 'http://image.path'),
(10, 'Song 5', 3, 'http://song.path', 'http://image.path');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_artist`
--

CREATE TABLE `tbl_artist` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `image` varchar(600) NOT NULL,
  `sex` enum('MALE','FEMALE','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_artist`
--

INSERT INTO `tbl_artist` (`id`, `name`, `image`, `sex`) VALUES
(1, 'A. R. Rahman', 'https://upload.wikimedia.org/wikipedia/commons/1/13/A.R.Rahman_at_57th_FF_Awards.jpg', 'MALE'),
(3, 'Arijit Singh', 'https://en.wikipedia.org/wiki/Arijit_Singh#/media/File:Arijit_Singh_Photo.jpg', 'MALE'),
(4, 'Alisha Chinai', 'https://upload.wikimedia.org/wikipedia/commons/5/5a/Alisha_Chinai_2009_-_still_64293_crop.jpg', 'FEMALE');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_artist_song`
--

CREATE TABLE `tbl_artist_song` (
  `id` int(11) NOT NULL,
  `artist_id` int(11) NOT NULL,
  `track_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_artist_song`
--

INSERT INTO `tbl_artist_song` (`id`, `artist_id`, `track_id`) VALUES
(1, 1, 1),
(2, 3, 2),
(3, 4, 3),
(7, 1, 2),
(8, 2, 6),
(9, 1, 2),
(10, 2, 6);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_top_list`
--

CREATE TABLE `tbl_top_list` (
  `id` int(11) NOT NULL,
  `track_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_top_list`
--

INSERT INTO `tbl_top_list` (`id`, `track_id`) VALUES
(1, 1),
(2, 2),
(5, 3),
(6, 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `playlist`
--
ALTER TABLE `playlist`
  ADD PRIMARY KEY (`playlist_id`);

--
-- Indexes for table `playlist_data`
--
ALTER TABLE `playlist_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `playlist_data_ibfk_1` (`playlist_id`);

--
-- Indexes for table `song`
--
ALTER TABLE `song`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_artist`
--
ALTER TABLE `tbl_artist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_artist_song`
--
ALTER TABLE `tbl_artist_song`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_top_list`
--
ALTER TABLE `tbl_top_list`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `playlist`
--
ALTER TABLE `playlist`
  MODIFY `playlist_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `playlist_data`
--
ALTER TABLE `playlist_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `song`
--
ALTER TABLE `song`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_artist`
--
ALTER TABLE `tbl_artist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_artist_song`
--
ALTER TABLE `tbl_artist_song`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_top_list`
--
ALTER TABLE `tbl_top_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `playlist_data`
--
ALTER TABLE `playlist_data`
  ADD CONSTRAINT `playlist_data_ibfk_1` FOREIGN KEY (`playlist_id`) REFERENCES `playlist` (`playlist_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
